using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndText : MonoBehaviour
{
    private Text resultText;
    void Start()
    {
        resultText = GetComponent<Text>();
        resultText.text = GameData.winOrLose;
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
