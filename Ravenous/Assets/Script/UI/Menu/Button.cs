using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    [SerializeField] private GameObject nextCanvas;
    [SerializeField] private GameObject mainMenuCanvas;
    [SerializeField] private Slider soundSlider;

    public void Play()
    {
        SceneManager.LoadScene("Playground");
    }
    public void ChangeMenu()
    {
        nextCanvas.SetActive(true);
        transform.parent.gameObject.SetActive(false);
    }
    public void Sound()
    {
       GameData.soundValue = soundSlider.value;
    }
    public void Quit()
    {
        Application.Quit();
    }
    public static void EndGame()
    {
        SceneManager.LoadScene("EndingScreen");
    }

}
