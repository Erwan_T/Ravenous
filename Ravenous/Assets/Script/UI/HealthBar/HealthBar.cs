using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Image healthBar;
    void Start()
    {
        healthBar = GetComponent<Image>();
    }
    public void SetHealthBar(float value)
    {
        healthBar.fillAmount = value;
    }
    void Update()
    {
        
    }
}
