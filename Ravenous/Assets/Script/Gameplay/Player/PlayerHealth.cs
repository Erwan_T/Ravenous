using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float maxHealth = 100f;
    [SerializeField] private HealthBar healthBar;
    private float actualHealth;
    void Start()
    {
        actualHealth = maxHealth;
        healthBar = GameObject.FindGameObjectWithTag("PlayerHealthBar").GetComponent<HealthBar>();
    }

    void Update()
    {
        if (actualHealth <= 0)
        {
            //Le Joueur est Mort
            Debug.Log("PlayerDead");
            GameData.winOrLose = "You're Dead";
            Button.EndGame();
        }
    }
    public void TakeDamage(float damageTaken)
    {
        actualHealth -= damageTaken;
        healthBar.SetHealthBar(actualHealth / maxHealth);
    }
}
