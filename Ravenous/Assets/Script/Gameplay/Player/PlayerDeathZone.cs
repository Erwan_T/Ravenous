using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathZone : MonoBehaviour
{
    private Vector3 startingPos;
    [SerializeField] private float fallingDamage;
    void Start()
    {
        startingPos = transform.position;
    }

    void Update()
    {
        if(transform.position.y < 0)
        {
            transform.position = startingPos;
            GetComponent<PlayerHealth>().TakeDamage(fallingDamage);
        }
    }
}
