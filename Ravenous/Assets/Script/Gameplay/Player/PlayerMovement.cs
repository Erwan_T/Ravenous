using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private Transform playerCamera;
    [SerializeField] private Rigidbody rb;

    [SerializeField] private float speed;

    [SerializeField] private float sensivityX;
    [SerializeField] private float sensivityY;
    [SerializeField] private float camLock;

    [SerializeField] private float jumpForce;

    [SerializeField] private float dashForce;
    [SerializeField] private float dashCooldown;

    [SerializeField] private float shootRange;
    [SerializeField] private float weaponCooldown;
    [SerializeField] private float weaponDamage;
    [SerializeField] private LayerMask ennemyMask;

    [SerializeField] private List<AudioClip> clips = new List<AudioClip>();

    private Vector3 lookPos;
    private Vector3 moveInput;
    private float dashRecoil;
    private float weaponRecoil;
    private float rotation;
    private bool isShooting;
    private void Awake()
    {
        player = new Player();
        playerCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();

        player.PlayerAction.Look.performed += ctx => Look(ctx);
        player.PlayerAction.Jump.performed += _ => Jump();
        player.PlayerAction.Dash.performed += _ => Dash();
        player.PlayerAction.Shoot.started += _ => isShooting = true;
        player.PlayerAction.Shoot.canceled += _ => isShooting = false;
        player.PlayerAction.Stop.performed += _ => Application.Quit();

    }
    private void OnEnable()
    {
        player.Enable();
    }
    private void OnDisable()
    {
        player.Disable();
    }
    void Update()
    {
        moveInput = player.PlayerAction.Move.ReadValue<Vector3>();
        transform.Translate(moveInput * speed * Time.deltaTime);
        dashRecoil -= Time.deltaTime;
        weaponRecoil -= Time.deltaTime;
        if (isShooting && weaponRecoil <= 0)
            Shoot();
    }
    private void Look(InputAction.CallbackContext ctx)
    {
        Vector2 context = ctx.ReadValue<Vector2>();
        transform.Rotate(Vector3.up, context.x * sensivityX);

        rotation -= context.y * sensivityY;
        rotation = Mathf.Clamp(rotation, -camLock, camLock);
        Vector3 targetRotation = transform.eulerAngles;
        targetRotation.x = rotation;
        playerCamera.eulerAngles = targetRotation;
    }
    private void Jump()
    { 
        if (Physics.Raycast(transform.position, -transform.up, 1.1f))
        {
            rb.AddRelativeForce(transform.up * jumpForce, ForceMode.Impulse);
            Debug.Log("Jump");
        }
    }
    private void Dash()
    {
        if (dashRecoil <= 0)
        {
            AudioSource.PlayClipAtPoint(clips[0], transform.position, GameData.soundValue);
            rb.AddRelativeForce(moveInput * dashForce , ForceMode.Impulse);
            dashRecoil = dashCooldown;
        }
    }
    private void Shoot()
    {
        AudioSource.PlayClipAtPoint(clips[1], transform.position, GameData.soundValue);
        RaycastHit hit;
        Physics.Raycast(transform.position, playerCamera.forward, out hit, shootRange, ennemyMask);
        if(hit.collider != null)
        {
            hit.collider.GetComponentInParent<BoidHealth>().TakeDamage(weaponDamage);
            Debug.Log(hit.collider.transform.name);
        }
        weaponRecoil = weaponCooldown;
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 50);
        Gizmos.DrawLine(transform.position,  playerCamera.forward * shootRange);
    }
}
