using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidsSound : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private AudioSource source;
    void Start()
    {
        source = GetComponent<AudioSource>();
        source.volume = GameData.soundValue;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
