using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidHealth : MonoBehaviour
{
    [SerializeField] private BoidsManager boidsManager;
    [SerializeField] public float maxHealth = 1f;
    [SerializeField] private bool isBoss = false;
    [SerializeField] private HealthBar healthBar;
    public float actualHealth { get; private set; }
    void Start()
    {
        boidsManager = GetComponentInParent<BoidsManager>();
        actualHealth = maxHealth;
        if(isBoss)
        healthBar = GameObject.FindGameObjectWithTag("BossHealthBar").GetComponent<HealthBar>();
    }

    void Update()
    {
        if (actualHealth <= 0)
        {
            if (isBoss)
            {
                GameData.winOrLose = "You Win";
                Button.EndGame();
            }
            else
            {
                boidsManager.RemoveBoid(GetComponent<Boid>());
                Destroy(gameObject);
            }
            
        }
            
    }
    public void TakeDamage(float damageTaken)
    {
        actualHealth -= damageTaken;
        if(isBoss)
            healthBar.SetHealthBar(actualHealth / maxHealth);
    }
}
