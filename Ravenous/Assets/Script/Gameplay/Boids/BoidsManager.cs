using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidsManager : MonoBehaviour
{
    //Variables
    [SerializeField] private Boid prefabBoids;
    [SerializeField] private Boid prefabBoss;
    [SerializeField] private int nbrBoids;
    [SerializeField] private int limiteBoid;
    [SerializeField] private float randomRange;
    public List<Boid> boids { get; private set; }
    private Vector3 startPos;
    void Start()
    {
        boids = new List<Boid>();
        Boid boss = Instantiate(prefabBoss, transform.position, Quaternion.Euler(0, 0, 0), transform);
        boids.Add(boss);
        AddBoid(nbrBoids, transform.position);
        Debug.Log(GameData.soundValue);
        Cursor.visible = false;
    }
    void Update()
    {
        
    }
    public void RemoveBoid(Boid thisBoid)
    {
        boids.Remove(thisBoid);
    }
    public void AddBoid(float nbrBoid, Vector3 addPos)
    {
        for (int i = 0; i < nbrBoid; i++)
        {
            if(boids.Count < limiteBoid)
            {
                Vector3 summonPos = (addPos + Random.insideUnitSphere) * 2;
                Boid clone = Instantiate(prefabBoids, summonPos, Quaternion.Euler(Random.Range(-randomRange, randomRange), Random.Range(-randomRange, randomRange), Random.Range(-randomRange, randomRange)), transform);
                boids.Add(clone);
            }
        }
    }
}
