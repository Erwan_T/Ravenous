using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour
{
    //Variables
    private BoidsManager boidsManager;
    [SerializeField] private Transform player;

    [SerializeField] private float zoneAlignement = 10;
    [SerializeField] private float zoneCohesion = 50;
    [SerializeField] private float zoneSeparation = 5;
    [SerializeField] private float zonePlayer = 30;

    [SerializeField] private float forceAlignement = 30;
    [SerializeField] private float forceCohesion = 30;
    [SerializeField] private float forceSeparation = 30;
    [SerializeField] private float forcePlayer = 30;
    [SerializeField] private float forceUp = 30;
    [SerializeField] private float bossForce = 10;

    [SerializeField] private float speed = 5f;
    [SerializeField] private Transform boidRoot;
    private Vector3 velocity;
    private Vector3 sumForce;
    private float boostForce;
    private float distance;
    private float distanceBtwZone;
    private float nbrForce; 

    [SerializeField] private bool drawGizmo = false;
    [SerializeField] private bool drawDebugLines = true;
    private Color colorDebugForce;
    void Start()
    {
        velocity = new Vector3();
        boidsManager = GetComponentInParent<BoidsManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    void Update()
    {
        nbrForce = 0;
        foreach (Boid otherBoid in boidsManager.boids)
        {
            if (otherBoid.tag == "Boss")
                boostForce = bossForce * boidsManager.boids.Count;
            else
                boostForce = 1;
            distance = Vector3.Distance(transform.position, otherBoid.transform.position);
            //Check voir si le boids detecte qqchose
            if (distance < zoneCohesion)
            {
                Vector3 posOtherBoid = otherBoid.transform.position - transform.position;

                //Si il est dans la zone de Cohesion
                if (distance > zoneAlignement)
                {
                    distanceBtwZone = ((distance - zoneAlignement) / (zoneCohesion - zoneAlignement)) * boostForce;
                    sumForce += posOtherBoid.normalized * forceCohesion * distanceBtwZone;
                    colorDebugForce = Color.green;
                }
                //Si il est dans la zone dAlignement
                else if (distance > zoneSeparation)
                {
                    sumForce += otherBoid.velocity.normalized * forceAlignement;
                    colorDebugForce = Color.yellow;
                }
                //Si il est dans la zone de Separation
                else
                {
                    distanceBtwZone = (1 - (distance / zoneSeparation)) * boostForce;
                    sumForce += posOtherBoid.normalized * -forceSeparation * distanceBtwZone;
                    colorDebugForce = Color.red;
                }

                nbrForce++;
            }
                //Amelioration des boids
            //Reste proche du joueur
            distance = Vector3.Distance(transform.position, player.position);
            if (distance > zonePlayer)
            {
                Vector3 posToPlayer = player.position - transform.position;
                distanceBtwZone = (distance - zonePlayer)/zonePlayer;
                sumForce += posToPlayer.normalized * forcePlayer * distanceBtwZone;
                nbrForce++;
                colorDebugForce = Color.black;
            }
            if (transform.position.y < player.position.y)
            {
                sumForce += transform.up * forceUp;
                nbrForce++;
            }
        }
        sumForce /= nbrForce;
        velocity += -velocity * 10 * Vector3.Angle(sumForce, velocity) / 180.0f * Time.deltaTime;
        velocity += sumForce * Time.deltaTime;
        transform.LookAt(transform.position + velocity.normalized);
        //transform.eulerAngles += new Vector3(0, 90, 0);
        transform.position += velocity.normalized * speed * Time.deltaTime;
    }

    private void OnDrawGizmos()
    {
        if (drawDebugLines)
        {
            Debug.DrawLine(transform.position, transform.position + velocity, colorDebugForce);
        }
        if (drawGizmo)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, zoneCohesion);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, zoneAlignement);

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, zoneSeparation);

        }
    }
}
