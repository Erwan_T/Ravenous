using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private float accurancy = 2f;
    [SerializeField] private float explosionRange = 3f;
    [SerializeField] private float damage = 3f;
    [SerializeField] private GameObject player;
    [SerializeField] private AudioClip clip;
    private Vector3 targetPos;
    void Start()
    {
        targetPos = new Vector3();
        player = GameObject.FindGameObjectWithTag("Player");
        targetPos = player.transform.position;
    }

    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
        //Si arrive a destination explose
        if(Vector3.Distance(transform.position, targetPos) <= accurancy || Vector3.Distance(transform.position,player.transform.position) <= explosionRange)
        {
            Debug.Log("Explose");
            AudioSource.PlayClipAtPoint(clip, transform.position, GameData.soundValue * 2);
            if (Vector3.Distance(transform.position,player.transform.position) <= explosionRange)
            {
                //Touche le joueur
                player.GetComponent<PlayerHealth>().TakeDamage(damage);
                Debug.Log("Touche");
            }
            Destroy(gameObject);
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRange);
    }
}
