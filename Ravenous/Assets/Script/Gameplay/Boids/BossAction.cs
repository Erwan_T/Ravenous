using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAction : MonoBehaviour
{
    [SerializeField] private BoidsManager boidsManager;
    [SerializeField] private BoidHealth boidHealth;
    [SerializeField] private Transform player;
    [SerializeField] private Fireball fireBall;
    [SerializeField] private List<AudioClip> clips = new List<AudioClip>();

    private float difficultyFactor;
    private float nbrFireball;
    private float nbrFireballMax;

    [SerializeField] private float summonCooldown = 1f;
    private float summonRecoil;    
    
    [SerializeField] private float fireBallCooldown = 3f;
    [SerializeField] private float fireBallSummonTime = 1f;
    private float fireBallRecoil;

    void Start()
    {
        boidsManager = GetComponentInParent<BoidsManager>();
        boidHealth = GetComponentInParent<BoidHealth>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        fireBallRecoil = fireBallCooldown;
        summonRecoil = summonCooldown;
    }

    void Update()
    {
        //Gestion de la diffculte croissante
        difficultyFactor = 1 + ((boidHealth.maxHealth - boidHealth.actualHealth) / 10);

        //Summon reguliere
        summonRecoil -= Time.deltaTime;
        if (summonRecoil <= 0)
        {
            AudioSource.PlayClipAtPoint(clips[1], transform.position,GameData.soundValue * 2);
            boidsManager.AddBoid(difficultyFactor * 4, transform.position);
            summonRecoil = summonCooldown;
        }

        //Fireball reguliere
        fireBallRecoil -= Time.deltaTime;
        if (fireBallRecoil <= 0)
        {
            AudioSource.PlayClipAtPoint(clips[0], transform.position, GameData.soundValue * 2);
            if(nbrFireball - 1 > 0)
            {
                Fireball clone = Instantiate(fireBall, transform.position,Quaternion.LookRotation(player.position - transform.position,Vector3.up));
                fireBallRecoil = fireBallSummonTime / nbrFireballMax;
                nbrFireball--;
            }
            else
            {
                Fireball clone = Instantiate(fireBall, transform.position, Quaternion.LookRotation(player.position - transform.position, Vector3.up));
                fireBallRecoil = fireBallCooldown;
                nbrFireballMax = difficultyFactor;
                nbrFireball = nbrFireballMax;
            }
            
            
            
        }

    }
}
